package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.UserLogoutRequest;
import ru.t1.vlvov.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "User logout.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

}
