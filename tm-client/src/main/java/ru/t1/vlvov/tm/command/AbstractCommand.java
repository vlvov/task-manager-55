package ru.t1.vlvov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.enumerated.Role;

@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void execute();

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String description = getDescription();
        @Nullable final String argument = getArgument();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    protected void setToken(@Nullable final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

}
