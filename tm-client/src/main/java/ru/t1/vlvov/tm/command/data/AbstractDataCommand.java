package ru.t1.vlvov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
