package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBinaryLoadRequest;

@Component
public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    private final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        domainEndpoint.loadDataBinary(request);
    }

}
