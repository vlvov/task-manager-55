package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBase64LoadRequest;

@Component
public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from base 64 file.";

    @NotNull
    private final String NAME = "data-load-text";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        domainEndpoint.loadDataBase64(request);
    }

}
