package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST Name: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
