package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.*;
import ru.t1.vlvov.tm.api.repository.ICommandRepository;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.exception.AbstractException;
import ru.t1.vlvov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.vlvov.tm.exception.system.CommandNotSupportedException;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.service.CommandService;
import ru.t1.vlvov.tm.service.LoggerService;
import ru.t1.vlvov.tm.service.PropertyService;
import ru.t1.vlvov.tm.service.TokenService;
import ru.t1.vlvov.tm.util.SystemUtil;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Modifier;
import java.util.Set;

@Getter
@Component
@ComponentScan("ru.t1.vlvov.tm")
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.vlvov.tm.command";

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    private void initCommands() {
        for (AbstractCommand command : abstractCommands) {
            commandService.add(command);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initCommands();
        initPID();
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        fileScanner.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.stop();
    }

    public void run(@NotNull final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

    private boolean processArguments(@Nullable final String args[]) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        if (argument == null) return false;
        processArgument(argument);
        return true;
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    private void processCommands() {
        String command;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("OK");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.err.println("FAIL");
            }
        }
    }

    private void processCommand(final String name) {
        processCommand(name, true);
    }

    public void processCommand(final String name, final boolean checkRole) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException(name);
        command.execute();
    }

}
