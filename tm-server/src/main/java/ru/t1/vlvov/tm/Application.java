package ru.t1.vlvov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.component.Bootstrap;
import ru.t1.vlvov.tm.service.PropertyService;

public class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
