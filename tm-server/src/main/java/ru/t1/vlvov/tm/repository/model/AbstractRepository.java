package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.model.IRepository;
import ru.t1.vlvov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    final protected EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

}
