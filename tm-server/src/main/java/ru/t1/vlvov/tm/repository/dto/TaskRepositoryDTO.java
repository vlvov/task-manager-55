package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class TaskRepositoryDTO extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM TaskDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final TaskDTO model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO WHERE userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE userId = :userId AND id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TaskDTO> criteriaQuery = criteriaBuilder.createQuery(TaskDTO.class);
        Root<TaskDTO> task = criteriaQuery.from(TaskDTO.class);
        criteriaQuery.select(task)
                .where(criteriaBuilder.equal(task.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(task.get(Sort.getOrderByField(sort))));
        TypedQuery<TaskDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAllByProjectId(@NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE projectId = :projectId AND userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
