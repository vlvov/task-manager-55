package ru.t1.vlvov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.UserDTO;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

}
