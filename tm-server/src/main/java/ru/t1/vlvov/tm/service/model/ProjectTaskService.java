package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.api.service.model.IProjectTaskService;
import ru.t1.vlvov.tm.api.service.model.ITaskService;
import ru.t1.vlvov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.vlvov.tm.exception.entity.TaskNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ITaskService getTaskService() {
        return new TaskService(connectionService);
    }

    @NotNull
    private IProjectService getProjectService() {
        return new ProjectService(connectionService);
    }

    @Override
    public void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectService projectService = getProjectService();
        Project project = getProjectService().findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final ITaskService taskService = getTaskService();
        Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectService projectService = getProjectService();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final ITaskService taskService = getTaskService();
        List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks != null)
            for (final Task task : tasks) {
                taskService.remove(task);
            }
        projectService.remove(project);
    }

    @Override
    public void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!getProjectService().existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final ITaskService taskService = getTaskService();
        Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final String taskProjectId = task.getProject().getId();
        if (taskProjectId == null || taskProjectId.isEmpty() || !taskProjectId.equals(projectId))
            throw new TaskNotFoundException();
        task.setProject(null);
        taskService.update(task);
    }

}
